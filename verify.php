<?php

include 'session_status.php';
if ( is_session_started() === FALSE ) session_start();

include 'config.php';

$username = $_POST["username"];
$password = $_POST["pwd"];
$url = $_GET["url"];
$flag = 'true';
//$query = $mysqli->query("SELECT email, password from users");

$result = $mysqli->query('SELECT id,email,password,fname,type from users order by id asc');

if($result === FALSE){
  die(mysql_error());
}

if($result){
  while($obj = $result->fetch_object()){
    if($obj->email === $username && $obj->password === $password) {

      $_SESSION['username'] = $username;
      $_SESSION['type'] = $obj->type;
      $_SESSION['id'] = $obj->id;
      $_SESSION['fname'] = $obj->fname;
      header("location:index.php");
    } else {

        if($flag === 'true'){
          redirect($url);
          $flag = 'false';
        }
    }
  }
}

function redirect($url) {
  if (!isset($url) or $url==='')
    { $url='index.php'; }
  echo '<h1>Invalid Login! Redirecting to '.$url.'...</h1>';
  header("Refresh: 3; url=$url");
}


?>
